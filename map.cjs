function cb(element) {
    return parseInt(element);
}

function map(elements, cb) {
    try {
        let result = [];
        let arr=[...elements];
        for(let index=0; index< elements.length; index++){
            result.push(cb(elements[index],index,arr));
        }
        return result;
    }
    catch (err) {
        return [];
    } 
    return [];
}


module.exports = map;