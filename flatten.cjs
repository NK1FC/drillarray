
function flatten(elements, depth = 1) {
    let arr = [];
    function flattenHelper(elements, currentDepth) {

        for (let element of elements) {

            if (Array.isArray(element) && currentDepth != 0) {
                flattenHelper(element, currentDepth - 1);
            } else if (element !== undefined) {
                arr.push(element)
            }

        }
    }

    flattenHelper(elements, depth);
    return arr;
}


module.exports=flatten;