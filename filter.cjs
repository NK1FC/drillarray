

function filter(elements, cb) {
    try {
        const result = [];
        const arr = [...elements];
        for (let index = 0; index < elements.length; index++) {
            if (cb(elements[index], index, arr) === true) {
                result.push(elements[index]);
            }
        }
        return result;
    }
    catch (err) {
        return [];
    } return [];
    // Do NOT use .filter, to complete this function.  // Similar to `find` but you will return an array of all elements that passed the truth tes    // Return an empty array if no elements pass the truth test
}


let words = ["spray", "limit", "exuberant", "destruction", "elite", "present"];

function dc(word, index, arr) {
    arr[index + 1] += " extra";
    return word.length < 6;
};

module.exports = filter;


console.log(words.filter(dc));
console.log(filter(words, dc));
