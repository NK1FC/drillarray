

function reduce(elements, cb, startingValue) {
    try {
        const arr = [...elements];
        for (let index = 0; index < elements.length; index++) {

            if (index === 0 && startingValue === undefined) {
                startingValue = elements[index];
                index++;
            }
            startingValue = cb(startingValue, elements[index], index, arr);
        }
        return startingValue
    }catch(err){
        return undefined;
    }return undefined;
}

module.exports=reduce;