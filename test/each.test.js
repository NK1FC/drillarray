const each = require('../each.cjs');

const items=[1,2,3,4,5];

function cb(element){
    console.log(element);
}

test('Each', ()=>{
    expect(each(items,cb)).toStrictEqual(items.forEach(cb));
});
