const filter = require('../filter.cjs');

const items=[1,2,3,4,5];

function cb(element){
    return element>2;
}

test('Filter', ()=>{
    expect(filter(items,cb)).toStrictEqual(items.filter(cb));
});
