const find = require('../find.cjs');

const items=[1,2,3,4,5];

function cb(element){
    return element==2;
}

test('Find', ()=>{
    expect(find(items,cb)).toStrictEqual(items.find(cb));
});
