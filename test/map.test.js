const map = require('../map.cjs');

const items=[1,2,3,4,5];

function cb(element){
    return element*element;
}

test('Map', ()=>{
    expect(map(items,cb)).toStrictEqual(items.map(cb));
});
