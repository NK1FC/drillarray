const reduce = require('../reduce.cjs');

const items=[1,2,3,4,5];

function cb(startingValue,element){
    startingValue+=element;
    return startingValue;
}



test('Reduce', ()=>{
    expect(reduce(items,cb)).toStrictEqual(items.reduce(cb));
});
